package cl.mobdev.demoandreacomposemvvm.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import cl.mobdev.demoandreacomposemvvm.model.CourseDetail
import cl.mobdev.demoandreacomposemvvm.model.CourseList
import cl.mobdev.demoandreacomposemvvm.model.CourseListItem
import cl.mobdev.demoandreacomposemvvm.repository.CourseDetailRepository
import cl.mobdev.demoandreacomposemvvm.repository.CourseListRepository
import cl.mobdev.demoandreacomposemvvm.repository.RetrofitClientRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CourseViewModel(
    application: Application,
    private val retrofitClientRepo: RetrofitClientRepository = RetrofitClientRepository(),
    private val courseListRepo: CourseListRepository = CourseListRepository(application),
    private val courseDetailRepo: CourseDetailRepository = CourseDetailRepository(application),
    private val log: String = "ERROR_CALL",
    var id: MutableLiveData<String> = MutableLiveData<String>(),
    val listCourseItem: LiveData<List<CourseListItem>> = courseListRepo.toListCourses(),
    val listCourseDetail: LiveData<List<CourseDetail>> = courseDetailRepo.toListDetails(),
    val course: MutableLiveData<CourseDetail> = MutableLiveData()
) : AndroidViewModel(application) {

    fun callCourseList() {
        retrofitClientRepo.getItem().enqueue(object : Callback<CourseList> {
            override fun onResponse(call: Call<CourseList>, response: Response<CourseList>) {
                response.body().let {
                    courseListRepo.addCourseList(it!!)
                }
            }

            override fun onFailure(call: Call<CourseList>, t: Throwable) {
                Log.e(log, t.message.toString())
            }
        })
    }

    fun callCourseDetail(id: String) {
        retrofitClientRepo.getDetail(id).enqueue(object : Callback<CourseDetail> {
            override fun onResponse(call: Call<CourseDetail>, response: Response<CourseDetail>) {
                response.body().let {
                    courseDetailRepo.addCourseDetail(it!!)
                }
            }

            override fun onFailure(call: Call<CourseDetail>, t: Throwable) {
                Log.e(log, t.message.toString())
            }
        })
    }

    fun searchCourses() {
        CoroutineScope(Dispatchers.IO).launch {
            course.postValue(courseDetailRepo.findCourseDetail(id.value!!))
        }
    }

}

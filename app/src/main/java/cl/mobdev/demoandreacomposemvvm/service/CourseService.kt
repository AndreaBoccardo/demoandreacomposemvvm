package cl.mobdev.demoandreacomposemvvm.service

import cl.mobdev.demoandreacomposemvvm.model.CourseDetail
import cl.mobdev.demoandreacomposemvvm.model.CourseList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CourseService {

    @GET("courses")
    fun getCourseList(): Call<CourseList>

    @GET("courses_details/{id}")
    fun getCourseDetail(@Path("id") id: String): Call<CourseDetail>
}
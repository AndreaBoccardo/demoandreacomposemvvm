package cl.mobdev.demoandreacomposemvvm.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import cl.mobdev.demoandreacomposemvvm.screens.DetailScreen
import cl.mobdev.demoandreacomposemvvm.screens.ListScreen

@Composable
fun AppNavigation(navController: NavHostController = rememberNavController()) {

    NavHost(navController = navController, startDestination = AppScreens.ListScreen.route) {
        composable(route = AppScreens.ListScreen.route) {
            ListScreen(navController)
        }
        composable(route = AppScreens.DetailScreen.route + "/{text}",
            arguments = listOf(navArgument(name = "text") {
                type = NavType.StringType
            })
        ) {
            DetailScreen(navController, it.arguments?.getString("text"))
        }
    }
}
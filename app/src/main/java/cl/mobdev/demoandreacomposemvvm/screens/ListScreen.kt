package cl.mobdev.demoandreacomposemvvm.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import cl.mobdev.demoandreacomposemvvm.R
import cl.mobdev.demoandreacomposemvvm.model.CourseListItem
import cl.mobdev.demoandreacomposemvvm.navigation.AppScreens
import cl.mobdev.demoandreacomposemvvm.viewmodel.CourseViewModel
import coil.compose.AsyncImage


@Composable
fun ListScreen(navController: NavController) {

    val viewModel: CourseViewModel = viewModel()
    val courses by viewModel.listCourseItem.observeAsState()

    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceAround
    ) {
        MyListOfCourses(courses!!, navController, onAddClick = {
            viewModel.callCourseDetail(viewModel.id.value!!)
        })
    }
}



@Composable
fun MyItem(course: CourseListItem, navController: NavController){
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .background(MaterialTheme.colors.onPrimary)
    ) {
        Column(
            modifier = Modifier
                .border(2.dp, Color.Red)
                .fillMaxWidth()
                .padding(8.dp)
                .background(MaterialTheme.colors.primary)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                AsyncImage(
                    model = course.image,
                    contentDescription = "course image")
                /*Image(
                    modifier = Modifier.size(200.dp),
                    painter = painterResource(course.image), contentDescription = "image")*/
                MyTexts(course, navController)
            }
        }
    }
}

@Composable
fun MyText(text: String, color: Color, style: TextStyle, fontWeight: FontWeight, lines: Int = Int.MAX_VALUE){
    Text(text, color = color, style = style, fontWeight = fontWeight, maxLines = lines)
}

@Composable
fun MyTexts(course: CourseListItem, navController: NavController){

    val viewModel: CourseViewModel = viewModel()

    var expanded by remember { mutableStateOf(false) }

    Column(modifier = Modifier
        .padding(start = 8.dp)
        .clickable {
            expanded = !expanded
        }) {
        course.title?.let {
            MyText(
                text = it,
                color = Color.Black,
                style = MaterialTheme.typography.subtitle2,
                fontWeight = FontWeight.Bold
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        course.previewDescription?.let {
            MyText(
                text = it,
                color = Color.Black,
                style = MaterialTheme.typography.subtitle2,
                fontWeight = FontWeight.Normal,
                if (expanded) Int.MAX_VALUE else 1
            )
        }
        Button(onClick = {
            navController.navigate(route = AppScreens.DetailScreen.route + "/${course.id}")
        }) {
            Text(stringResource(cl.mobdev.demoandreacomposemvvm.R.string.button_detail))
        }
    }
}

@Composable
fun MyListOfCourses(myCourses: List<CourseListItem>, navController: NavController, onAddClick: (() -> Unit)? = null){
    LazyColumn {
        items(myCourses){course ->
            MyItem(course, navController)
        }
    }
}

package cl.mobdev.demoandreacomposemvvm.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

class CourseList : ArrayList<CourseListItem>()


@Entity(tableName = "course_list")
data class CourseListItem(

    @SerializedName("image")
    val image: String?,

    @SerializedName("weeks")
    val weeks: Int?,

    @SerializedName("previewDescription")
    val previewDescription: String?,

    @SerializedName("start")
    val start: String?,

    @SerializedName("id")
    @PrimaryKey val id: String,

    @SerializedName("title")
    val title: String?
)

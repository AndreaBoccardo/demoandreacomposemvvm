package cl.mobdev.demoandreacomposemvvm.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "course_detail")
data class CourseDetail(

    @SerializedName("image")
    val image: String?,

    @SerializedName("weeks")
    val weeks: Int?,

    @SerializedName("modality")
    val modality: String?,

    @SerializedName("start")
    val start: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("scholarshipsAvailable")
    val scholarshipsAvailable: Boolean?,

    @SerializedName("id")
    @PrimaryKey val id: String,

    @SerializedName("tuition")
    val tuition: String?,

    @SerializedName("title")
    val title: String?,

    @SerializedName("minimumSkill")
    val minimumSkill: String?
)

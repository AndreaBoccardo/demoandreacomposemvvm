package cl.mobdev.demoandreacomposemvvm.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import cl.mobdev.demoandreacomposemvvm.dao.CourseDetailDao
import cl.mobdev.demoandreacomposemvvm.dao.CourseListDao
import cl.mobdev.demoandreacomposemvvm.model.CourseDetail
import cl.mobdev.demoandreacomposemvvm.model.CourseListItem

@Database(entities = [CourseListItem::class, CourseDetail::class], version = 1)
abstract class ProjectDatabase : RoomDatabase() {

    abstract fun courseListDao(): CourseListDao
    abstract fun courseDetailDao(): CourseDetailDao

    companion object {
        @Volatile
        private var instance: ProjectDatabase? = null

        fun getInstance(context: Context): ProjectDatabase {
            if (instance == null) {
                synchronized(this)
                {
                    instance = Room.databaseBuilder(
                        context,
                        ProjectDatabase::class.java,
                        "proyecto_db"
                    )
                        .build()
                }
            }
            return instance!!
        }
    }
}
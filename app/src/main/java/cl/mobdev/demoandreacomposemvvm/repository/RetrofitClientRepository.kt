package cl.mobdev.demoandreacomposemvvm.repository

import cl.mobdev.demoandreacomposemvvm.client.RetrofitClient
import cl.mobdev.demoandreacomposemvvm.model.CourseDetail
import cl.mobdev.demoandreacomposemvvm.model.CourseList
import cl.mobdev.demoandreacomposemvvm.service.CourseService
import retrofit2.Call

class RetrofitClientRepository(
    private val client: CourseService = RetrofitClient.getInstance(RetrofitClient.base_url)
) {

    fun getItem(): Call<CourseList> {
        return client.getCourseList()
    }

    fun getDetail(id: String): Call<CourseDetail> {
        return client.getCouseDetail(id)
    }
}
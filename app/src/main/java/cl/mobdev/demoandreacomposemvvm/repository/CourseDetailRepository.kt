package cl.mobdev.demoandreacomposemvvm.repository

import android.content.Context
import androidx.lifecycle.LiveData
import cl.mobdev.demoandreacomposemvvm.model.CourseDetail
import cl.mobdev.demoandreacomposemvvm.room.ProjectDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CourseDetailRepository(
    var context: Context,
    private val db: ProjectDatabase = ProjectDatabase.getInstance(context)
) {

    fun addCourseDetail(courseDetail: CourseDetail) {
        CoroutineScope(Dispatchers.IO).launch {
            db.courseDetailDao().addCourseDetail(courseDetail)
        }
    }

    fun toListDetails(): LiveData<List<CourseDetail>> {
        return db.courseDetailDao().toListDetails()
    }

    fun findCourseDetail(id: String): CourseDetail {
        return db.courseDetailDao().findCourseDetail(id)
    }

}
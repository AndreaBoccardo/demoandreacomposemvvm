package cl.mobdev.demoandreacomposemvvm.repository

import android.content.Context
import androidx.lifecycle.LiveData
import cl.mobdev.demoandreacomposemvvm.model.CourseList
import cl.mobdev.demoandreacomposemvvm.model.CourseListItem
import cl.mobdev.demoandreacomposemvvm.room.ProjectDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CourseListRepository(
    var context: Context,
    private val db: ProjectDatabase = ProjectDatabase.getInstance(context)
) {

    fun addCourseList(courseList: CourseList) {
        CoroutineScope(Dispatchers.IO).launch {
            db.courseListDao().addCourseList(courseList)
        }
    }

    fun toListCourses(): LiveData<List<CourseListItem>> {
        return db.courseListDao().toListCourses()
    }
}
package cl.mobdev.demoandreacomposemvvm.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import cl.mobdev.demoandreacomposemvvm.model.CourseList
import cl.mobdev.demoandreacomposemvvm.model.CourseListItem

@Dao
interface CourseListDao {

    @Insert(onConflict = REPLACE)
    fun addCourseList(courseList: CourseList)

    @Query("select id, image, weeks, previewDescription, start, title from course_list")
    fun toListCourses(): LiveData<List<CourseListItem>>
}

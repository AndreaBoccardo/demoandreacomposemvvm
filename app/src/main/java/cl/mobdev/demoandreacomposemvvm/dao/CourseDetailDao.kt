package cl.mobdev.demoandreacomposemvvm.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import cl.mobdev.demoandreacomposemvvm.model.CourseDetail

@Dao
interface CourseDetailDao {

    @Insert(onConflict = REPLACE)
    fun addCourseDetail(courseDetail: CourseDetail)

    @Query("select id, image, weeks, modality, start, description, scholarshipsAvailable, tuition, " +
            "title, minimumSkill from course_detail")
    fun toListDetails(): LiveData<List<CourseDetail>>

    @Query("select id, image, weeks, modality, start, description, scholarshipsAvailable, tuition, " +
            "title, minimumSkill from course_detail where id = :id")
    fun findCourseDetail(id: String): CourseDetail

}

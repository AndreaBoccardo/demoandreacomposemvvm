package cl.mobdev.demoandreacomposemvvm.ui.theme

import android.annotation.SuppressLint
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@SuppressLint("ConflictingOnColor")
private val DarkColorPalette = darkColors(
    primary = Blue3,
    primaryVariant = Blue5,
    secondary = Blue2,
    secondaryVariant = Blue4,
    background = GrayDark,
    surface = Blue3,
    error = Blue1,
    onPrimary = Blue1,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Blue5,
    onError = Color.White
)

@SuppressLint("ConflictingOnColor")
private val LightColorPalette = lightColors(
    primary = Blue2,
    primaryVariant = Blue4,
    secondary = Blue3,
    secondaryVariant = Blue5,
    background = GrayLight,
    surface = Blue2,
    error = Blue1,
    onPrimary = Blue1,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Blue4,
    onError = Color.White
)

@Composable
fun DemoAndreaComposeMVVMTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )

    val systemUiController = rememberSystemUiController()
    if (darkTheme) {
        systemUiController.setSystemBarsColor(
            color = Blue5
        )
    } else {
        systemUiController.setSystemBarsColor(
            color = Blue4
        )
    }
}
package cl.mobdev.demoandreacomposemvvm.ui.theme

import androidx.compose.ui.graphics.Color

val GrayLight = Color(0xffBDBDBD)
val GrayDark = Color(0xff757575)
val Blue1 = Color(0xFFb9ffff)
val Blue2 = Color(0xFF81c9fa)
val Blue3 = Color(0xFF2196f3)
val Blue4 = Color(0xFF1465bb)
val Blue5 = Color(0xFF003785)





package cl.mobdev.demoandreacomposemvvm

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import cl.mobdev.demoandreacomposemvvm.navigation.AppNavigation
import cl.mobdev.demoandreacomposemvvm.ui.theme.DemoAndreaComposeMVVMTheme
import cl.mobdev.demoandreacomposemvvm.viewmodel.CourseViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val viewModel: CourseViewModel by viewModels()
            DemoAndreaComposeMVVMTheme {
                AppNavigation()
                viewModel.callCourseList()
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    DemoAndreaComposeMVVMTheme {
        AppNavigation()
    }
}